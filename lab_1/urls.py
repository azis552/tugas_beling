from django.urls import re_path
from .views import index
from .views import index, message_post
from .views import index, message_post, message_table  
#url for app
app_name = 'lab_1'
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^message_post', message_post, name='message_post'),
    re_path(r'^result_table', message_table, name='result_table')

]
