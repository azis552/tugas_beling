from django.shortcuts import render
from datetime import datetime, date
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message

# Enter your name here
mhs_name = 'Abdul Azis Ansori' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998,4,15) #TODO Implement this, format (Year, Month, Date)
pondok_mhs = 'INNAYATULLAH'
univ = 'UTY'
npm = 5170311053 # TODO Implement this

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm ,'pondok':pondok_mhs, 'univ':univ}
    response['message_form'] = Message_Form
    return render(request, 'index_lab1.html', response)
def table(request):
    response = {'name': 'Message'}
    response['message'] = messages
    return render(request, 'table.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
       response = {}
       response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
       response['email'] = request.POST['email'] if request.POST['email']!= "" else "Anonymous"
       response['message'] = request.POST['message']
       message = Message(name=response['name'], email=response['email'], 
       message=response['message'])
       message.save()
       html ='form_result.html'
       return render(request, html, response)
    else:
       return HttpResponseRedirect('/')
def message_table(request):
    response = {}
    message = Message.objects.all() 
    response['message'] = message 
    html = 'table.html'
    return render(request, html , response)
