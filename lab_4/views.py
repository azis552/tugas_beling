from django.shortcuts import render

# Biodata
name_mhs = 'ABDUL AZIS ANSORI'
univ_mhs = 'UTY'
pondok_mhs = 'INNAYATULLAH'
# Create your views here.
def index(request):
	response = {'nama':name_mhs, 'univ':univ_mhs, 'pondok':pondok_mhs }
	return render(request, 'lab_4.html', response)
